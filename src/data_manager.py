#################################################################################
#                                                                               #
# Copyright 2016 Luca Antonelli                                                 #
# luke.anto@gmail.com                                                           #
#                                                                               #
# This file is part of Iris Predictor.                                          #
#                                                                               #
# Iris Predictor is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by          #
# the Free Software Foundation, either version 3 of the License, or             #
# (at your option) any later version.                                           #
#                                                                               #
# Iris Predictor is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of                #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                 #
# GNU General Public License for more details.                                  #
#                                                                               #
# You should have received a copy of the GNU General Public License             #
# along with Iris Predictor.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                               #
#################################################################################

import pandas as pd
import numpy as np

class DataLoader:
  """General class to load Data."""
  
  def __init__(self, columnNames):
    self._columnNames = columnNames
    self._data = None
    
  def load(self, path):
    """Load data into a numpy array."""
    newPath = self._preLoad(path)
    data = self._load(path)
    data = self._postLoad(data)
    self._data = data
    return data
  
  def _preLoad(self, path):
    """Operations to do before loadina data. May include direct file manipulation.
       Return the path of the new data file.
    """
    # Override in derived classes
    return path
  
  def _load(self, path):
    """Load data from path."""
    temp = pd.read_csv(path,
                       names = self._columnNames,
                       iterator = True,
                       chunksize = 1000)
    data = pd.concat(temp, ignore_index = True)
    return data
  
  def _postLoad(self, data):
    """Operation to do after loading data. May include data transformation and mapping."""
    # Override in derived classes
    return data


class IrisDataLoader(DataLoader):
  """Data loader for diabetes data."""
  
  columnNames = "SepalLength", "SepalWidth", "PetalLength", "PetalWidth", "Class"
  
  def __init__(self):
    super().__init__(IrisDataLoader.columnNames)

if __name__ == "__main__":
  import os
  
  dataLoader = IrisDataLoader()
  df = dataLoader.load(path = os.path.join("data","iris.data"))
  print("I read {0:d} rows of data.".format(len(df.index)))

